package com.wigroup.flutterzzendesk.flutterzendesk;

import android.app.Activity;
import android.content.Context;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import zendesk.chat.Chat;
import zendesk.chat.ChatConfiguration;
import zendesk.chat.ChatEngine;
import zendesk.chat.ChatProvidersConfiguration;
import zendesk.chat.VisitorInfo;
import zendesk.core.AnonymousIdentity;
import zendesk.core.Identity;
import zendesk.core.Zendesk;
import zendesk.messaging.Engine;
import zendesk.messaging.MessagingActivity;
import zendesk.support.Support;
import zendesk.support.SupportEngine;
import zendesk.support.Guide;
import zendesk.answerbot.AnswerBot;
import zendesk.answerbot.AnswerBotEngine;

public class ZendeskModule {
    private static String chatAccountKey;

    public static void initZendesk(Context context, MethodCall call, MethodChannel.Result result) {

        String zendeskUrl = call.argument("zendeskUrl");
        String applicationId = call.argument("applicationId");
        String oauthClientId = call.argument("oauthClientId");
        String chatAccountKey = call.argument("chatAccountKey");

        assert zendeskUrl != null;
        assert applicationId != null;

        Zendesk.INSTANCE.init(context, zendeskUrl, applicationId, oauthClientId);
        assert chatAccountKey != null;

        ZendeskModule.chatAccountKey = chatAccountKey;

        result.success("Initialised Zendesk sdk");
    }

    public static void showChatActivity(Activity activity, MethodCall call) {
        String fullName = call.argument("fullName");
        String email = call.argument("email");
        String phoneNumber = call.argument("phoneNumber");

        Support.INSTANCE.init(Zendesk.INSTANCE);
        Guide.INSTANCE.init(Zendesk.INSTANCE);
        AnswerBot.INSTANCE.init(Zendesk.INSTANCE, Guide.INSTANCE);

        Chat.INSTANCE.init(activity, chatAccountKey);

        VisitorInfo visitorInfo = VisitorInfo.builder()
                .withName(fullName)
                .withEmail(email)
                .withPhoneNumber(phoneNumber) // numeric string
                .build();

        ChatProvidersConfiguration chatProvidersConfiguration = ChatProvidersConfiguration.builder()
                .withVisitorInfo(visitorInfo)
                .build();

        Chat.INSTANCE.setChatProvidersConfiguration(chatProvidersConfiguration);

        Identity identity = new AnonymousIdentity.Builder()
                .withNameIdentifier(fullName)
                .withEmailIdentifier(email).build();

        Zendesk.INSTANCE.setIdentity(identity);

        ChatConfiguration chatConfiguration = ChatConfiguration.builder()
                .withPreChatFormEnabled(false)
                .build();

        Engine answerBotEngine = AnswerBotEngine.engine();
        Engine supportEngine = SupportEngine.engine();
        Engine chatEngine = ChatEngine.engine();

        MessagingActivity.builder()
                .withEngines(answerBotEngine, chatEngine, supportEngine)
                .withBotLabelString("Spot Help Bot")
                .withToolbarTitle("Get help")
                .show(activity, chatConfiguration);
    }
}
