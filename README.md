# flutterzendesk

Flutter plugin for Zendesk SDK

## Getting Started

- 1. Declare a Map with your values and pass it to initialize method in app's main
  static const Map<String, String> zendeskCredentials = {
    "zendeskUrl": "YOUR_ZENDESK_URL",
    "applicationId": "YOUR_ZENDESK_APPLICATION_ID",
    "oauthClientId": "mobile_sdk_client_YOUR_CLIENT_NUMBER",
    "chatAccountKey": "YOUR_ZENDESK_CHAT_API_KEY"
  };

Flutterzendesk.initialize(zendeskCredentials);

- 2. 
Start a chat : Declare a Map with your values and pass it to startChat method

  static const Map<String, String> _chatProps = {
    "fullName": " John Doe",
    "email": "johnDoe@gmail.com",
    "phoneNumber": "7654321",
    "mixpanelDistinctID": "123Mixpanel",
  };
  
Flutterzendesk.startChat(chatProps);