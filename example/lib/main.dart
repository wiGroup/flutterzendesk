import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutterzendesk/flutterzendesk.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  static const Map<String, String> _zendeskCredentials = {
    "zendeskUrl": "https://virginmoney.zendesk.com",
    "applicationId": "37e7e6aa02c7b3dac397ebc4114359e55f77b562a117d048",
    "oauthClientId": "mobile_sdk_client_318a1d25c691f02e5c7f",
    "chatAccountKey": "EceZOVwZZMDwJIhfi3jt2I5pZM7h3pdk"
  };

  static const Map<String, String> _chatProps = {
    "fullName": " John Doe",
    "email": "johnDoe@gmail.com",
    "phoneNumber": "7654321",
    "mixpanelDistinctID": "123Mixpanel",
  };

  @override
  void initState() {
    super.initState();
    initZendesk();
  }

  Future<void> initZendesk() async {
    String initialiseRes;
    try {
      initialiseRes = await Flutterzendesk.initialize(_zendeskCredentials);
    } on PlatformException {
      initialiseRes = 'Failed to get platform version.';
    }
    if (!mounted) return;

    setState(() {
      _platformVersion = initialiseRes;
    });
  }

  Future _startChat(Map<String, String> chatProps) async {
    // await Flutterzendesk.startChat(null); //For unauthenticated user
    await Flutterzendesk.startChat(_chatProps);
  }

  _button(String title, Function onPressed) {
    return RaisedButton(
      color: Color.fromARGB(255, 134, 250, 194),
      textColor: Color.fromARGB(255, 33, 28, 92),
      child: Padding(
          padding: EdgeInsets.all(16),
          child: Text(
            title,
            style: TextStyle(fontSize: 16),
          )),
      onPressed: onPressed,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side: BorderSide(color: Color.fromARGB(255, 134, 250, 194))),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 246, 246, 246),
        appBar: AppBar(
          title: const Text('Zendesk| Demo app'),
          backgroundColor: Color.fromARGB(255, 55, 63, 67),
        ),
        body: Center(
          child: ListView(
              padding: EdgeInsets.all(20.0),
              shrinkWrap: true,
              children: <Widget>[
                _button("Start Chat", () => _startChat(_chatProps)),
                SizedBox(height: 20),
              ]),
        ),
        bottomSheet: Container(
          padding: EdgeInsets.all(10.0),
          color: Colors.white,
          width: double.infinity,
          child: Text("Developed with ♥ by Leonardo Lerasse"),
        ),
      ),
    );
  }
}
