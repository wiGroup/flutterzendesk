import 'dart:async';

import 'package:flutter/services.dart';

class Flutterzendesk {
  static const MethodChannel _channel = const MethodChannel('flutterzendesk');

  static Future initialize(Map<String, String> zendeskCredentials) async {
    return _channel.invokeMethod('initialize', zendeskCredentials);
  }

  static Future startChat(Map<String, String> props) async {
    await _channel.invokeMethod('startChat', props);
  }
}
