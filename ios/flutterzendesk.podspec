#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flutterzendesk.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutterzendesk'
  s.version          = '0.0.1'
  s.summary          = 'Flutter plugin for Zendesk SDK'
  s.description      = <<-DESC
Flutter plugin for Zendesk SDK.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.dependency 'ZendeskAnswerBotSDK', '2.1.3'
  s.dependency 'ZendeskChatSDK', '2.11.1'
  s.dependency 'ZendeskSupportSDK', '5.3.0'
  s.platform = :ios, '11.0'

  # Flutter.framework does not contain a i386 slice. Only x86_64 simulators are supported.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
  s.swift_version = '5.0'
end
