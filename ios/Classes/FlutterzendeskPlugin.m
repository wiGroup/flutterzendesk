#import "FlutterzendeskPlugin.h"
#if __has_include(<flutterzendesk/flutterzendesk-Swift.h>)
#import <flutterzendesk/flutterzendesk-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutterzendesk-Swift.h"
#endif

@implementation FlutterzendeskPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterzendeskPlugin registerWithRegistrar:registrar];
}
@end
