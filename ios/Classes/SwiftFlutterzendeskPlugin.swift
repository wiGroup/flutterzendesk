import Flutter
import UIKit

import ZendeskCoreSDK
import SupportProvidersSDK
import ChatSDK
import ChatProvidersSDK
import MessagingSDK
import MessagingAPI
import AnswerBotProvidersSDK
import AnswerBotSDK
import SupportSDK

public class SwiftFlutterzendeskPlugin: NSObject, FlutterPlugin {
    var flutterResult: FlutterResult?
    var zendeskChatAccountKey: String = ""

    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "flutterzendesk", binaryMessenger: registrar.messenger())
        let instance = SwiftFlutterzendeskPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        if (call.method == "initialize") {
            if let args = call.arguments as? Dictionary<String, Any>,
                let zendeskUrl = args["zendeskUrl"] as? String,
                let applicationId = args["applicationId"] as? String,
                let oauthClientId = args["oauthClientId"] as? String,
                let chatAccountKey = args["chatAccountKey"] as? String
            {
                print("Zendesk initializing...");
                Zendesk.initialize(appId: applicationId, clientId: oauthClientId, zendeskUrl: zendeskUrl)
                self.zendeskChatAccountKey = chatAccountKey
                print("Zendesk initialized...")
                result("Zendesk ios Initialized with url: \(zendeskUrl)")
            }
            
        } else if (call.method == "startChat") {
            
            DispatchQueue.main.async {
                self.flutterResult = result
                print("Zendesk starting chat...")

                do {
                    let chatViewController = try self.configureChat(call: call)
                    print("Zendesk chat configured")

                    let viewController = UINavigationController(rootViewController: chatViewController)
                    viewController.modalPresentationStyle = .fullScreen
                    
                    let button = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(self.dismiss))
                    
                    chatViewController.navigationItem.leftBarButtonItem = button
                    UIApplication.shared.keyWindow?.rootViewController?.present(viewController, animated: true, completion: nil)
                } catch {
                    print("Zendesk Unexpected error: \(error).")
                    result(FlutterError(code: error.localizedDescription , message: nil, details: nil))
                }
            }
        } else {
            result(FlutterMethodNotImplemented)
        }
    }

    func configureChat(call: FlutterMethodCall) throws -> UIViewController {
        print("Zendesk configuring chat");

        Support.initialize(withZendesk: Zendesk.instance)
        AnswerBot.initialize(withZendesk: Zendesk.instance, support: Support.instance!)
        Messaging.initialize()

        Chat.initialize(accountKey: self.zendeskChatAccountKey)

        if let args = call.arguments as? Dictionary<String, Any>,
            let fullName = args["fullName"] as? String,
            let email = args["email"] as? String,
            let phoneNumber = args["phoneNumber"] as? String
        {
            //configure visitor info
            let chatAPIConfiguration = ChatAPIConfiguration()

            chatAPIConfiguration.department = "Spot Support" //workaround for bug where customer not being added to a queue
            //set visitorInfo
            chatAPIConfiguration.visitorInfo = VisitorInfo(name: fullName, email: email, phoneNumber: phoneNumber)

            //set chat configuration
            Chat.instance?.configuration = chatAPIConfiguration
            print("Zendesk chat config complete")

            //configure identity
            let anonymous = Identity.createAnonymous(name: fullName, email: email)

            //set identity
            Zendesk.instance?.setIdentity(anonymous)
            print("Zendesk Identity set")
        }

        //configure messaging
        let messagingConfiguration = MessagingConfiguration()
        messagingConfiguration.name = "Spot Help Bot"

        let chatConfiguration = ChatConfiguration()
        chatConfiguration.isPreChatFormEnabled = false
        chatConfiguration.isOfflineFormEnabled = true
        chatConfiguration.isAgentAvailabilityEnabled = true
        chatConfiguration.chatMenuActions = [.emailTranscript, .endChat]

        //start engines
        let answerBotEngine = try AnswerBotEngine.engine()
        let supportEngine = try SupportEngine.engine()
        let chatEngine = try ChatEngine.engine()
        let viewController = try Messaging.instance.buildUI(engines: [answerBotEngine, chatEngine, supportEngine],
                                                            configs: [messagingConfiguration, chatConfiguration])
        print("Zendesk viewController set")

        return viewController

    }
    
    @objc func dismiss() {
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: {
            UIApplication.shared.keyWindow?.rootViewController?.view.endEditing(true)
        })
    }
}
